import React from 'react'
import { Redirect, Route } from "react-router-dom"
var success = localStorage.getItem('success');
var authToken 
export const PrivateRoute = ({ component: Component, ...rest }) => {
  if(success==="success"){
    authToken = true
  }else{
    authToken = false
  }
  console.log(success)
  return (
    <Route {...rest} render={props => (
      authToken ?
          <Component {...props} />
      : <Redirect to="/login" />
  )}
  />
  )
}

export default PrivateRoute
