import React from "react";
import { Link } from "react-router-dom";

  export default function HomeTable(props) {
    var username = localStorage.getItem('username');
    console.log(username);
    const tableList = () => {
      if (props.user.data !== undefined) {
      var data = [];
      for (let i = 0; i < props.user.data.length; i++) {
        let item = props.user.data[i];
        if(item.username === username ){
          data.push(
            <table className="table" >
              <thead>
                <tr>
                  <tr>{item.name}</tr>
                  <tr>{item.age}</tr>
                  <tr>{item.salary}</tr>
                  <tr>
                  <Link to={`/editprofile/${item._id}`}>
                    <span style={{ color: "red" }}>Edit</span>
                  </Link>
                  
                  </tr>
                </tr>
              </thead>
            </table>
          );
        }
        
      }
    return data;
    }
  };

  return (
    <div>
      <table className="table table-striped table-light" >
        <thead>
            <td>
              <img src={process.env.PUBLIC_URL+'assets/images/user.png'} width="150"></img>
            </td>
            <td >
              <tr >Name :</tr>
              <tr >Age :</tr>
              <tr >Salary :</tr>
              <tr >Actions :</tr>
            </td>
            <td >
              {tableList()}
            </td>
          </thead>
        
      </table>
      
    </div>
  );
}