import React from "react";
import { Link } from "react-router-dom";

  export default function MyProduct (props) {
    var u_id = localStorage.getItem('Id');
    var key = localStorage.getItem('key');
    const tableList = () => {
      if (props.product.data !== undefined) {
      var data = [];
      let counti = 0;
      let count = 0;
      for (let i = 0; i < props.product.data.length; i++) {
        let item = props.product.data[i];
        if(item.user_id === u_id){
            counti = counti+1;
            console.log(item.user_id);
            if(item.title.includes(key)|| item.detail.includes(key)){
              count = count+1
              data.push(
                <tr>
                  <td >{counti}</td>
                  <td>{item.title}</td>
                  <td>{item.detail}</td>
                  <td>{item.stock}</td>
                  <td>{item.price}</td>
                  <td>
                  <Link to={`/editproduct/${item._id}`}>
                    <button type="submit" className="btn btn-success">Edit</button>
                  </Link> &nbsp;
                  <button onClick={() => props.delete(item._id)} className="btn btn-danger">Delete</button>
                  <Link to={`/detail/${item._id}`}>&nbsp;
                    <button type="submit" className="btn btn-primary">View</button>
                  </Link>
                  </td>
                </tr>
              );
            }
            else if (key=== ""){
              count = count+1
              data.push(
                <tr>
                  <td >{counti}</td>
                  <td>{item.title}</td>
                  <td>{item.detail}</td>
                  <td>{item.stock}</td>
                  <td>{item.price}</td>
                  <td>
                  <Link to={`/editproduct/${item._id}`}>
                    <button type="submit" className="btn btn-success">Edit</button>
                  </Link> &nbsp;
                  <button onClick={() => props.delete(item._id)} className="btn btn-danger">Delete</button>&nbsp;
                  <Link to={`/detail/${item._id}`}>
                    <button type="submit" className="btn btn-primary">View</button>
                  </Link>
                  </td>
                </tr>
              );
            }
        }else{

        }
        
      }
      if(count === 0 ){
        data.push(
          <tr>
            <td >-</td>
            <td>ไม่พบข้อมูล</td>
            <td>ไม่พบข้อมูล</td>
            <td>ไม่พบข้อมูล</td>
            <td>ไม่พบข้อมูล</td>
          </tr>
        );

    }else{}
    return data;
    }
  };

  return (
    <div>
        <h3>My Product</h3>
      <table style={{ textAlign: "center" }}  class="table table-striped table-dark">
        <thead>
          <tr>
            <th scope="col">No.</th>
            <th scope="col">Title</th>
            <th scope="col">Detail</th>
            <th scope="col">Stock</th>
            <th scope="col">Price</th>
            <th scope="col">Actions</th>
          </tr>
          </thead>
        <tbody>{tableList()}</tbody>
      </table>
    </div>
  );
}