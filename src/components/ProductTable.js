import React from "react";
export default function ProductTable(props) {
  var key = localStorage.getItem('key');
    const tableList = () => {
      if (props.product.data !== undefined ) {
      var data = [];
      let count = 0;
      for (let i = 0; i < props.product.data.length; i++) {
        let item = props.product.data[i];
        if(item.title===key|| item.detail=== key){
          count = count+1
          data.push(
            <tr>
              <td >{i + 1}</td>
              <td>{item.title}</td>
              <td>{item.detail}</td>
              <td>{item.stock}</td>
              <td>{item.price}</td>
            </tr>
          );
        }
        else if (key=== ""){
          count = count+1
          data.push(
            <tr>
              <td >{i + 1}</td>
              <td>{item.title}</td>
              <td>{item.detail}</td>
              <td>{item.stock}</td>
              <td>{item.price}</td>
            </tr>
          );
        }
        
      }
      if(count === 0 ){
          data.push(
            <tr>
              <td >-</td>
              <td>ไม่พบข้อมูล</td>
              <td>ไม่พบข้อมูล</td>
              <td>ไม่พบข้อมูล</td>
              <td>ไม่พบข้อมูล</td>
            </tr>
          );

      }else{}
    return data;
    }
  };

  return (
    <div>
      <h3>All Product</h3>
      <table style={{ textAlign: "center" }} class="table table-striped table-dark ">
        <thead>
          <tr>
            <th scope="col">No.</th>
            <th scope="col">Title</th>
            <th scope="col">Detail</th>
            <th scope="col">Stock</th>
            <th scope="col">Price</th>
          </tr>
          </thead>
        <tbody>{tableList()}</tbody>
      </table>
    </div>
  );
}