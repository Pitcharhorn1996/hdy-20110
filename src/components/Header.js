import React from 'react'
import { NavLink } from 'react-router-dom'

export default function Header() {
  // var username = localStorage.getItem('username');
  return (
    <div>
        <nav className="navbar navbar-expand-lg navbar-light bg-dark">
          {/* <a className="navbar-brand text-white" href="#">{username}</a> */}
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>

          

          <div className="collapse navbar-collapse" id="navbarNav">
            <ul className="navbar-nav ml-auto">
              <li className="nav-item active">
                {/* <a class="nav-link" href="#">Login</a> */}
                <NavLink activeStyle={{ color: 'red' }} className="nav-link text-white" to="/login">Login</NavLink>
              </li>
              <li className="nav-item">
                {/* <a class="nav-link" href="#">Register</a> */}
                <NavLink activeStyle={{ color: 'red' }} className="nav-link text-white" to="/register">Register</NavLink>
              </li>
            </ul>
          </div>
        </nav>
    </div>
  )
}
