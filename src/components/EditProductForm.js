import React, { useState, useEffect } from 'react'

export default function EditProductForm(props) {
    const [title, setTitle] = useState('')
    const [detail, setDetail] = useState('')
    const [stock, setStock] = useState(0)
    const [price, setPrice] = useState(0)

 
    const edit = async (e) => {
        e.preventDefault()
        let product = {
          user_id:localStorage.getItem('Id'),
          title: title,
          detail: detail,
          stock: stock,
          price: price
        }
      props.edit(product)
      }
    
      useEffect(() => {
          setTitle(props.product.title)
          setDetail(props.product.detail)
          setStock(props.product.stock)
          setPrice(props.product.price)
        
      },[])

  return (
    <div>
      <form onSubmit={edit }>
        <div class="form-group">
          <label for="title">Title</label>
          <input type="text" value={title} onChange={(e) => setTitle(e.target.value)} class="form-control" id="title" />
        </div>
        <div class="form-group">
          <label for="detail">Datail</label>
          <input type="text" value={detail} onChange={(e) => setDetail(e.target.value)} class="form-control" id="detail"/>
        </div>
        <div class="form-group">
          <label for="stock">Stock</label>
          <input type="number" value={stock} onChange={(e) => setStock(e.target.value)} class="form-control" id="stock"/>
        </div>
        <div class="form-group">
          <label for="price">Price</label>
          <input type="number" value={price} onChange={(e) => setPrice(e.target.value)} class="form-control" id="price"/>
        </div>
        <button type="submit" class="btn btn-success btn-block">Edit</button>
      </form>
    </div>
  )
}
