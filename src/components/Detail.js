import React, { useEffect, useState } from "react";
import Back from "./Back";
import { getAllProduct } from "../api/api";

export default function EditProduct(props) {
  const [product, setProduct] = useState([]);

    const fetchApi = async () => {
      let result = await getAllProduct(props.match.params.id)
      let data = result.data.filter((items) => {
          return items._id === props.match.params.id;
        });
      
    setProduct(data[0]);
   
    }
        fetchApi();

  return (
<div><br></br><hr/>
    <h3>Product : {product.title}</h3><hr/>
    <table style={{ textAlign: "center" }} class="table table-striped table-dark ">
      <thead>
        <tr>
          <th scope="col">Title</th>
          <th scope="col">ID</th>
          <th scope="col">Detail</th>
          <th scope="col">Stock</th>
          <th scope="col">Price</th>
        </tr>
        </thead>
        <tbody>
        <tr>
          <th scope="col">{product.title}</th>
          <th scope="col">{product._id}</th>
          <th scope="col">{product.detail}</th>
          <th scope="col">{product.stock}</th>
          <th scope="col">{product.price}</th>
        </tr>
        </tbody>
    </table>
  </div>
  );
}
