import React from 'react';
import Header from './components/Header'
import Detail from './components/Detail'
import Login from './page/Login/Login'
import Register from './page/Register/Register'
import Home from './page/Home/Home'
import Create from './page/Create/Create'
import EditProfile from './page/Edit/EditProfile'
import EditProduct from './page/Edit/EditProduct'
import './App.css';
import { Route, Switch, Redirect } from 'react-router-dom'
import PrivateRoute from './helper/PrivateRoute'

var routes = {
  login: '/login',
  register: '/register',
  home: '/home',
  create: '/create',
  editprofile: '/editprofile/:id',
  editproduct: '/editproduct/:id',
  detail:'/detail/:id'
}

function App() {
  return (
    <div >
      <Header/>
      <div className="container login">
          <Switch>
            <Redirect exact from="/" to={routes.login}></Redirect>
            <Route exact path={routes.login} component={Login}></Route>
            <Route exact path={routes.register} component={Register}></Route>
          </Switch>
      </div>
      <div className="container">
          <Switch>
              <PrivateRoute exact path={routes.home} component={Home}></PrivateRoute>
              <PrivateRoute exact path={routes.create} component={Create}></PrivateRoute>
              <PrivateRoute exact path={routes.editprofile} component={EditProfile}></PrivateRoute>
              <PrivateRoute exact path={routes.editproduct} component={EditProduct}></PrivateRoute>
              <PrivateRoute exact path={routes.detail} component={Detail}></PrivateRoute>
          </Switch>
      </div>
    </div>
  );
}

export default App;
