import React, { useState} from "react";
import { register } from "../../api/api";
export default function Register(props) {
  const [username, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const [name, setName] = useState("");
  const [age, setAge] = useState(0);
  const [salary, setSalary] = useState(0);

  const save = async (e) => {
    e.preventDefault();
    let user = {
      username: username,
      password: password,
      name: name,
      age: age,
      salary: salary
    };
    let result = await register(user)
    props.history.push('/login')

    // console.log(save);
    
  }
  return (
    <div>
      {/* <h1 style={{ textAlign: "center" }}>REGISTER</h1> */}<br></br><br></br>
      <div style={{ textAlign: "center" }}>
        <img src={process.env.PUBLIC_URL+'assets/images/regis.png'} width="150"></img>
      </div>
        
      <form onSubmit={ save}>
        <div class="form-group">
          <label>Username:</label>
          <input type="text" onChange={(e) => setUserName(e.target.value)} class="form-control" id="username" />
        </div>
        <div class="form-group">
          <label>Password:</label>
          <input type="password" onChange={(e) => setPassword(e.target.value)} class="form-control" id="password" />
        </div>
        <div class="form-group">
          <label>Name:</label>
          <input type="text" onChange={(e) => setName(e.target.value)} class="form-control" id="name" />
        </div>
        <div class="form-group">
          <label>Age:</label>
          <input type="number" onChange={(e) => setAge(e.target.value)} class="form-control" id="age" />
        </div>
        <div class="form-group">
          <label>Salary:</label>
          <input type="number" onChange={(e) => setSalary(e.target.value)} class="form-control" id="salary" />
        </div>
        <button type="submit" class="btn btn-danger btn-block">submit</button>
      </form>
    </div>
  );
}