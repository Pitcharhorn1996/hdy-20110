
import React, { useState } from "react";
// import { login } from "../../api/api";
import { loginTest } from "../../api/api";
// import "./Login.css";
export default function Login(props) {
  // const [user, setUser] = useState();
  const [username, setUserName] = useState("");
    const [password, setPassword] = useState("");

      const login = async (e) => {
        e.preventDefault();
        let user = {
          username: username,
          password: password
        };
        let result = await loginTest(user)
        if(result.status === "success"){
          localStorage.setItem('Id', result.data._id);
          localStorage.setItem('username', result.data.username);
          localStorage.setItem('success', "success");
          
          // console.log(result.data._id);
          // console.log(result.data.username);
         props.history.push('/home')
         
        }
    
      };

  return (
    <div>
      <h1 style={{ textAlign: "center"}}>LOGIN</h1>
      <div style={{ textAlign: "center" }}>
        <img src={process.env.PUBLIC_URL+'assets/images/login.png'} width="150"></img>
      </div>

      <form onSubmit={login }>
        <div className="form-group">
          <label>Username:</label>
          <input type="text" onChange={(e) => setUserName(e.target.value)} className="form-control" id="username" />
        </div>
        <div className="form-group">
          <label>Password:</label>
          <input type="password" onChange={(e) => setPassword(e.target.value)} className="form-control" id="password" />
        </div>
        <button type="submit" className="btn btn-success btn-block">submit</button>
      </form>

    </div>
  );
}