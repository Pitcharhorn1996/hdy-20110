import React, { useEffect, useState } from "react";
import EditProductForm from "../../components/EditProductForm";
import Back from "../../components/Back";
import { getAllProduct, editProduct } from "../../api/api";

export default function EditProduct(props) {
  const [product, setProduct] = useState();

  useEffect(() => {
    const fetchApi = async () => {
      let result = await getAllProduct(props.match.params.id)
      let data = result.data.filter((items) => {
          return items._id === props.match.params.id;
        });
      console.log(data[0])
      setProduct(data[0]);
    }
    fetchApi();
  }, []);

  
  const edit = async (product) => {
    let edit = await editProduct(props.match.params.id, product)
    console.log(edit.status)
    if (edit.status === "success") {
      props.history.push('/home')
    } else{
        alert(edit.message)

    }
  }

  return (
    <div>
      <Back url="/home" history={props.history} />
      <h1>Edit Product</h1>
      <hr />
      {product !== undefined && <EditProductForm  product={product} edit={edit} />}
    </div>
  );
}
