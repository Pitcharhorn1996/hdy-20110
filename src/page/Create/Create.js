import React from 'react'
import CreateForm from '../../components/CreateForm'
import Back from '../../components/Back'
import { createProduct } from '../../api/api'

export default function Create(props) {

  const save = async (product) => {
    let result = await createProduct(product)
    props.history.push('/home')
  }
  return (
    <div>
      <Back url="/home" history={props.history}/>
      <h1>Create Product</h1>
      <hr/>
      <CreateForm check="Create" save={save}/>
    </div>
  )
}
