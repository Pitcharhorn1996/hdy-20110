import React,{useState,useEffect} from 'react'
import Profiletable from '../../components/ProfileTable'
import ProfileTitle from '../../components/ProfileTitle'
import Producttable from '../../components/ProductTable'
// import ProductTitle from '../../components/ProductTitle'
import MyProduct from '../../components/MyProduct'
import { getAllUser,deleteProduct,getAllProduct } from '../../api/api'
import ProductCreateButton from '../../components/ProductCreateButton'

export default function Home(props) {
    const [user,setUser] = useState({})
    const [product,setProduct] = useState({})
    const [key, setKey] = useState("");

    const fetchUser = async () => {
        let result = await getAllUser()
        setUser(result)
    }
    const fetchProduct = async () => {
        let result = await getAllProduct()
        setProduct(result)
    }

    useEffect(() => {
        fetchProduct();
        fetchUser();
    },[])

    const nextCreate = () => {
        props.history.push('/create')
    }

    const removeProduct = async (id) => {
        const check = window.confirm("คุณต้องการลบหรือไม่ ?")
        if(check === true){
            let result = await deleteProduct(id)
            if(result.status === "success"){
                fetchProduct()
            }
        }
    }

    const search = async () => {
        //  e.preventDefault();
          localStorage.setItem('key', key);
         
        }
    

    return (
        <div>
            
            <ProfileTitle/>
            <Profiletable user={user}/><br></br>
            <hr  size="20" color="red" align="center"></hr><br></br>
                <ul className="nav nav-pills mb-3" id="pills-tab" role="tablist">
                <li className="nav-item">
                    <a className="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">All Product</a>
                </li>
                <li className="nav-item">
                    <a className="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">My Product</a>
                </li>
                
                </ul>
                <form  className="form-inline justify-content-md-end" onSubmit={search}>
                <input  type="search" onChange={(e) => setKey(e.target.value)} className="form-control" id="title" placeholder="Search" aria-label="Search"></input>
                    <button  class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                </form>
                <div className="tab-content" id="pills-tabContent">
                    <div className="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab"><Producttable product={product} /></div>
                    <div className="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab"><MyProduct product={product} delete={removeProduct}/></div>
                </div>
            
            <ProductCreateButton nextCreate={nextCreate}/>
        </div>
    )

}